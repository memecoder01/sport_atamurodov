import datetime
from django.db import models
from admintion.models import BaseModel
from users.models import NameModel
from crm.service import hafta_kunlarini_chiqar
from users.models import CustomUser
from datetime import datetime
from dateutil.relativedelta import relativedelta    
from django.db.models import F, Sum
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
# Create your models here.
class SportType(BaseModel):
    name = models.CharField(max_length=300)
    
    class Meta:
        verbose_name = "Sport tur"
        verbose_name_plural = "Sport turlari"
    def __str__(self):
        return self.name
    
class ClassStudent(BaseModel):
    name = models.CharField(max_length=300)
    
    class Meta:
        verbose_name = "Talaba sinf"
        verbose_name_plural = "Talaba sinflari"
    def __str__(self):
        return self.name
class Teacher(BaseModel):
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    def groups_num(self):
        return Group.objects.filter(teacher=self).count()
    
    def groups(self):
        return Group.objects.filter(teacher=self).all()
    def subjects(self):
        return Subject.objects.filter(teacher=self).all()
    def groups_student_info(self):
        g = self.groups()
        c = []
        for s in g:
            active_students = s.students_count()
            l = {
                'name': s.name,
                'active_students': active_students,
            }
            c.append(l)
        return c
    
    def __str__(self):
        return self.user.full_name
class Subject(BaseModel):
    sport_type = models.ForeignKey(SportType, on_delete=models.CASCADE)
    class_student = models.ForeignKey(ClassStudent, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Fan"
        verbose_name_plural = "Fanlar"

    def lessons(self):
        return Lesson.objects.filter(subject=self).all().order_by('order')
    def __str__(self):
        return self.name
    
class Lesson(BaseModel):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    order = models.PositiveSmallIntegerField(default=0)
    class Meta:
        verbose_name = "Dars"
        verbose_name_plural = "Darslar"
    def __str__(self):
        return self.name
    def resurses(self):
        return Resurs.objects.filter(lesson=self).all()
    def tasks(self):
        return Task.objects.filter(lesson=self).all()
    def check_tasks(self):
        return CheckTask.objects.filter(task__lesson=self).all()
class ResursFiles(BaseModel):
    file = models.FileField(upload_to='filetest/')
    
    class Meta:
        verbose_name = "Resurs Fayl"
        verbose_name_plural = "Resurs Fayllar"
        
class Resurs(BaseModel):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    files = models.ManyToManyField(ResursFiles)
    class Meta:
        verbose_name = "Resurs"
        verbose_name_plural = "Resurslar"
    def __str__(self):
        return self.name
class Task(BaseModel):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    files = models.FileField(upload_to='file/tasks/')
    class Meta:
        verbose_name = "Topshiriq"
        verbose_name_plural = "Topshiriqlar"
    def __str__(self):
        return self.name    

    
class StudentClass(NameModel):
    def groups(self):
        return Group.objects.filter(st_class=self).all()
    def groups_count(self):
        return Group.objects.filter(st_class=self).all().count()
    def __str__(self):
        return self.name



 
class Room(NameModel):
    pass

class Group(NameModel):
    st_class = models.ForeignKey(StudentClass, on_delete=models.SET_NULL,null=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL,null=True)
    room = models.ForeignKey(Room, on_delete=models.SET_NULL,null=True)
   
    def days(self):
        return Day.objects.filter(group=self).all()
    def tasks(self):
            tasks = []
        # try: 
            ss = Teacher.objects.get(id=self.id).subjects()
            for s in ss:
                ls = s.lessons()
                for l in ls:
                    for g in l.tasks():
                        tasks.append(g)

            return tasks
        # except: return tasks
   
    def students_count(self):
        return Student.objects.filter(group=self).count()
    
    def students(self):
        return Student.objects.filter(group=self).all()
    
     
   
class Day(NameModel):
    date = models.DateField()
    group = models.ForeignKey(Group, on_delete=models.CASCADE,null=True)
    def __str__(self):
        return str(self.group.name) + str(self.date)        
        
        


class ParentType(NameModel):
    pass

class Parent(BaseModel):
    full_name = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    type = models.ForeignKey(ParentType,on_delete=models.CASCADE)

class Student(BaseModel):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    group = models.ManyToManyField(Group,blank=True) 
    parent = models.ForeignKey(Parent,on_delete=models.CASCADE,null=True,blank=True)
    class_student = models.ForeignKey(ClassStudent, on_delete=models.CASCADE,null=True,blank=True)
    sport_type = models.ForeignKey(SportType, on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.user.full_name
    class Meta:
        verbose_name = "Talaba"
        verbose_name_plural = "Talabalar"
    
    def save(self, *args, **kwargs):
        super(Student, self).save(*args, **kwargs)     
        
class Attedence(BaseModel):
    day = models.ForeignKey(Day,on_delete=models.SET_NULL,null=True)
    student = models.ForeignKey(Student,on_delete=models.CASCADE)
    is_has  = models.BooleanField(null=True,blank=True)
    
    def __str__(self):
        return self.student.user.full_name + ' ' + str(self.day)
    
 

class StudentNote(BaseModel):
    student = models.ForeignKey(Student,on_delete=models.CASCADE)
    text = models.TextField()
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    is_view = models.BooleanField(default=False)
    
    def __str__(self):
        return  str(self.student.user.full_name)+ " " + self.text
    

class CheckTask(BaseModel):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    student = models.ForeignKey(Student,on_delete=models.CASCADE)
    baho = models.CharField(max_length=200,null=True,blank=True)
    file = models.FileField(upload_to="task/check/",null=True,blank=True)
    comment = models.CharField(max_length=200,null=True,blank=True)
    is_check = models.BooleanField(default=False)
    class Meta:
        verbose_name = "Topshiriq tekshirish"
        verbose_name_plural = "Topshiriqlarni tekshirish"
    def __str__(self):
        return str(self.task.name)   
    

