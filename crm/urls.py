
from django.urls import path

from crm.views import *

urlpatterns = [
    path('', home, name="home"),
    path('teachers/', teachers, name="teachers"),
    path('teacher/<int:id>/detail/', teacher_detail, name="teacher_detail"),
    path('groups/', groups, name="groups"),
    path('group_detail/<int:id>/detail/', group_detail, name="group_detail"),
    path('students/', students, name="students"),
    path('student/<int:id>/detail/', student_detail, name="student_detail"),
    path('add-student/', add_student, name="add_student"),
    path('task_check/<int:id>/', task_check, name="task_check"),
    path('task_send/<int:id>/', task_send, name="task_send"),
    path('add-group/', add_group, name="add_group"),
    path('add_content/<int:id>/', content, name="add_content"),
    path('add-teacher/', add_teacher, name="add_teacher"),
    path('students-state/', students_state, name="students_state"),
    path('course-effectiveness/', course_effectiveness, name="course_effectiveness"),
    path('students-days/', students_days, name="students_days"),
    path('courses/', courses, name="courses"),
    path('course_detail/<int:id>/', course_detail, name="course_detail"),
    path('subject/<int:id>/detail/', subject_detail, name="subject_detail"),
    path('lesson/<int:id>/detail/', lesson_detail, name="lesson_detail"),
    path('rooms/', rooms, name="rooms"),
    path('attendance_report/', attendance_report, name="attendance_report"),
   
    

]