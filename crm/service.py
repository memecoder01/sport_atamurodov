from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
def hafta_kunlarini_chiqar(boshlangich_sana, oy,hafta_kunlari):
    boshlangich_sanasi = boshlangich_sana
    tugash_sanasi = boshlangich_sanasi + relativedelta(months=+oy)
    kunlar_massivi = []

    while boshlangich_sanasi <= tugash_sanasi:
        # Kunning hafta kunlarini tekshiramiz (0 - dushanba, 2 - seshanba, 4 - juma)
        if boshlangich_sanasi.weekday() in hafta_kunlari:
            kunlar_massivi.append(boshlangich_sanasi.strftime('%Y-%m-%d'))
        
        # Keyingi kunni hisoblash
        boshlangich_sanasi += timedelta(days=1)

    return kunlar_massivi

# Test qismi
# boshlangich_sana = '22.11.2023'
# natija = hafta_kunlarini_chiqar(boshlangich_sana, 3, [0,3,5])
# print(natija)
