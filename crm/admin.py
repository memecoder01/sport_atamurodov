from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from .models import *

@admin.register(Day)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Day._meta.fields]
    list_filter = [f.name for f in Day._meta.fields]
    history_list_display = ["status"]

@admin.register(Attedence)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Attedence._meta.fields]
    list_filter = [f.name for f in Attedence._meta.fields]
    history_list_display = ["status"]
@admin.register(Task)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Task._meta.fields]
    list_filter = [f.name for f in Task._meta.fields]
    history_list_display = ["status"]
@admin.register(CheckTask)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in CheckTask._meta.fields]
    list_filter = [f.name for f in CheckTask._meta.fields]
    history_list_display = ["status"]
@admin.register(Teacher)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Teacher._meta.fields]
    list_filter = [f.name for f in Teacher._meta.fields]
    history_list_display = ["status"]
    
    # prepopulated_fields = {'slug':('name',),}
@admin.register(StudentClass)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in StudentClass._meta.fields]
    list_filter = [f.name for f in StudentClass._meta.fields]
    history_list_display = ["status"]
    # prepopulated_fields = {'slug':('name',),}




@admin.register(Room)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Room._meta.fields]
    list_filter = [f.name for f in Room._meta.fields]
    history_list_display = ["status"]
    # prepopulated_fields = {'slug':('name',),}

@admin.register(Group)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Group._meta.fields]
    list_filter = [f.name for f in Group._meta.fields]
    history_list_display = ["status"]
    # prepopulated_fields = {'slug':('name',),}



@admin.register(ParentType)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in ParentType._meta.fields]
    list_filter = [f.name for f in ParentType._meta.fields]
    history_list_display = ["status"]
    # prepopulated_fields = {'slug':('name',),}

@admin.register(Parent)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Parent._meta.fields]
    list_filter = [f.name for f in Parent._meta.fields]
    history_list_display = ["status"]
    # prepopulated_fields = {'slug':('name',),}

@admin.register(Student)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Student._meta.fields]
    list_filter = [f.name for f in Student._meta.fields]
    history_list_display = ["status"]
    # prepopulated_fields = {'slug':('name',),}



@admin.register(StudentNote)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in StudentNote._meta.fields]
    list_filter = [f.name for f in StudentNote._meta.fields]
    history_list_display = ["status"]

@admin.register(SportType)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in SportType._meta.fields]
    list_filter = [f.name for f in SportType._meta.fields]
    history_list_display = ["status"]

@admin.register(ClassStudent)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in ClassStudent._meta.fields]
    list_filter = [f.name for f in ClassStudent._meta.fields]
    history_list_display = ["status"]

@admin.register(Subject)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Subject._meta.fields]
    list_filter = [f.name for f in Subject._meta.fields]
    history_list_display = ["status"]

@admin.register(Lesson)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Lesson._meta.fields]
    list_filter = [f.name for f in Lesson._meta.fields]
    history_list_display = ["status"]


@admin.register(ResursFiles)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in ResursFiles._meta.fields]
    list_filter = [f.name for f in ResursFiles._meta.fields]
    history_list_display = ["status"]

@admin.register(Resurs)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in Resurs._meta.fields]
    list_filter = [f.name for f in Resurs._meta.fields]
    history_list_display = ["status"]

