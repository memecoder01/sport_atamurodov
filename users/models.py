from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.utils.translation import gettext_lazy as _

from simple_history.models import HistoricalRecords
import uuid



class CustomUserManager(BaseUserManager):
    def create_user(self, phone, password, **extra_fields):
        if not phone:
            raise ValueError("Phone fields is required")
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_active', True)
        user = self.model(phone=phone, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, phone, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_superuser', True)

        return self.create_user(phone, password, **extra_fields)
# Create your models here.

class CustomUser(AbstractBaseUser, PermissionsMixin):
    id = models.AutoField(unique=True,primary_key=True)
    phone = models.CharField(
        _('phone'), max_length=200, unique=True
    )
    full_name = models.CharField(max_length=500,null=True,blank=True)
    birthday = models.DateField(null=True, blank=True)
    image = models.ImageField(null=True, blank=True,upload_to='users/',default='static/assets/img/user.png')
    email = models.EmailField(max_length=200,null=True,blank=True,verbose_name='Email')
    address = models.CharField(max_length=500,null=True,blank=True)
    passport = models.CharField(max_length=500,null=True,blank=True)
    parol = models.CharField(max_length=500,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    last_seen = models.DateTimeField(blank=True, null=True)
    history = HistoricalRecords(inherit=True)
    is_director = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_student = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    class Meta:
        ordering = ("created_at",)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()
    def __str__(self):
        return self.phone
    
    def get_absolute_url(self):
        return f'/users/{self.id}'

    def get_update_url(self):
        return f'/users/{self.id}/update'

    def get_delete_url(self):
        return f'/users/{self.id}/delete'


class NameModel(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    history = HistoricalRecords(inherit=True)
    # admin_user = models.ForeignKey(CustomUser,on_delete=models.PROTECT,null=True,blank=True,editable=False,related_name="Adminuser")
    name = models.CharField(max_length=500)
    
    def __str__(self):
        return self.name
    
    class Meta:
        abstract = True
        
        
