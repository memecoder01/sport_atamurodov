from django.db import models
from admintion.models import BaseModel
from ckeditor.fields import RichTextField
from users.models import CustomUser
# Create your models here.
class Logo(BaseModel):
    title = models.CharField(max_length=200,null=True)
    content = models.TextField(null=True)
    logo = models.ImageField(upload_to='logo/')
    logocentre = models.ImageField(upload_to='logo/')
    logocircle = models.ImageField(upload_to='logo/')
    preloder_style = models.TextField(null=True)

    class Meta:
        verbose_name = 'Asosiy ma\'lumotlar'
        verbose_name_plural = 'Asosiy ma\'lumotlar'


class HomeInfo(BaseModel):
    name = models.CharField(max_length=100)
    name2 = models.CharField(max_length=100, blank=True, null=True)
    content = models.CharField(max_length=500)
    btn_name = models.CharField(max_length=50, blank=True, null=True)
    btn_url = models.CharField(max_length=500, blank=True, null=True)
    vid_name = models.CharField(max_length=50, blank=True, null=True)
    vid_url = models.CharField(max_length=500, blank=True, null=True)
    image = models.ImageField(upload_to='images/home/')
    image_phone = models.ImageField(upload_to='images/home/', blank=True, null=True)

    def __str__(self) -> str:
        return self.name
    
    class Meta:
        verbose_name = 'Bosh sahifa ma\'lumotlar'
        verbose_name_plural = 'Bosh sahifa ma\'lumotlar'
    

class Social(BaseModel):
    icon = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    url = models.URLField()

    def __str__(self) -> str:
        return self.name
    class Meta:
        verbose_name = 'Ijtimoiy tarmoq'
        verbose_name_plural = 'Ijtimoiy tarmoqlar'
class HomeFAQ(BaseModel):
    image = models.ImageField(upload_to='images/homeFAQ/')
    q1 = models.CharField(max_length=500)
    an1 = models.TextField()
    q2 = models.CharField(max_length=500)
    an2 = models.TextField()
    q3 = models.CharField(max_length=500)
    an3 = models.TextField()

    def __str__(self) -> str:
        return self.q1
    class Meta:
        verbose_name = 'Bosh sahifa uchun savol-javob'
        verbose_name_plural = 'Bosh sahifa uchun savol-javoblar'
class Contact(BaseModel):
    phone = models.CharField(max_length=50)
    phone2 = models.CharField(max_length=50,null=True, blank=True)
    email = models.EmailField()
    email2 = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=500)
    google_map = models.TextField()
    
    def __str__(self) -> str:
        return self.phone
    
    class Meta:
        verbose_name = 'Bog\'lanish uchun ma\'lumot'
        verbose_name_plural = 'Bog\'lanish uchun ma\'lumotlar'
    

class AboutAS(BaseModel):
    image = models.ImageField(upload_to='images/aboutus/')
    content = models.CharField(max_length=500,null=True, blank=True)
    full_name = models.CharField(max_length=100)
    position = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.full_name


class Management(BaseModel):
    full_name = models.CharField(max_length=150)    
    position = models.CharField(max_length=150)  
    phone = models.CharField(max_length=150)  
    email = models.EmailField(max_length=150)  
    telegram = models.URLField(max_length=150,null=True)  
    facebook = models.URLField(max_length=150,null=True)  
    image = models.ImageField(upload_to='images/managment/')
    order = models.IntegerField(default=0)

    def __str__(self) -> str:
        return self.full_name
    
    class Meta:
        verbose_name = 'Rahbariyat'
        verbose_name_plural = 'Rahbariyat'
    
class UsefullLinks(BaseModel):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='images/usefull/')
    url = models.URLField()

    def __str__(self) -> str:
        return self.name
    
    class Meta:
        verbose_name = 'Foydali havolalar yoki hamkor'
        verbose_name_plural = 'Foydali havolalar yoki hamkorlar'
class Direction(BaseModel):
    name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.name
    
    class Meta:
        verbose_name = 'Yo\'nalishlar'
        verbose_name_plural = 'Yo\'nalishlar'
    
class ATInfo(BaseModel):
    img = models.ImageField(upload_to='images/info/')
    img2 = models.ImageField(upload_to='images/info/')
    img3 = models.ImageField(upload_to='images/info/')
    title = models.CharField(max_length=500)
    content = models.TextField()
    directions = models.ManyToManyField(Direction)

    def __str__(self):
        return self.title
    class Meta:
        verbose_name = 'Yo\'nalishlar haqida ma\'lumot'
        verbose_name_plural = 'Yo\'nalishlar haqida ma\'lumot'

class Numbers(BaseModel):
    icon = models.CharField(max_length=100,blank=True,null=True)
    image = models.ImageField(upload_to='images/info/',blank=True,null=True)
    nums = models.PositiveSmallIntegerField(default=0)
    name = models.CharField(max_length=250)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Statistika'
        verbose_name_plural = 'Statistikalar'
    
 
class EventTag(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
       return  self.name
    def natural_key(self):
        return self.name
    class Meta:
        verbose_name = "hashtag"
class EventCategory(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
       return self.name
    def natural_key(self):
        return self.name
    class Meta:
        verbose_name = "Tadbir kategoriyasi"

    def event_nums(self):
        a = Events.objects.filter(category_name=self.name).all().count()

event_type = (
    ('yangilik', 'yangilik'),
    ('elon', 'elon'),
    ('tanlov', 'tanlov'),
)

class Events(BaseModel):
    type = models.CharField(max_length=200,choices=event_type)
    category = models.ForeignKey(EventCategory,on_delete=models.CASCADE, verbose_name="Kategoriyasi")
    image = models.ImageField(upload_to='events/', verbose_name="Rasmi")
    date = models.DateTimeField()
    user = models.ForeignKey(CustomUser,on_delete=models.PROTECT)
    title = models.CharField(max_length=200, verbose_name="Sarlavhasi")
    sub_title =  models.CharField(max_length=400, verbose_name="Qisqa mazmuni", blank=True, null=True)
    content = RichTextField(verbose_name="Mazmuni")
    hash_tags = models.ManyToManyField(EventTag, verbose_name="Hash tag")
    publish = models.BooleanField(default=True)
    slug = models.SlugField(unique=True, verbose_name="Slug")
    class Meta:
        verbose_name = 'Tadbir'
        verbose_name_plural = 'Tadbirlar'
    def __str__(self):
        return self.title

class Projects(BaseModel):
    image = models.ImageField(upload_to='projects/')
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=500)
    url = models.URLField()
    class Meta:
        verbose_name = 'IT proyekt'
        verbose_name_plural = 'IT proyektlar'

class CoursesDirection(BaseModel):
    name = models.CharField(max_length=150)
    class Meta:
        verbose_name = 'Kurs yo\'nalishi'
        verbose_name_plural = 'Kurs yo\'nalishlari'

    def __str__(self) -> str:
        return self.name
class Courses(BaseModel):
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=100,null=True, blank=True)
    url = models.URLField()
    directions = models.ManyToManyField(CoursesDirection)
    class Meta:
        verbose_name = 'Kurslar'
        verbose_name_plural = 'Kurslar'

    def __str__(self) -> str:
        return self.name
    

class Statictics(BaseModel):
    name = models.CharField(max_length=150)
    prosent = models.PositiveSmallIntegerField(default=50)

    def __str__(self) -> str:
        return self.name

class HomeStasInfo(models.Model):
    min_content = models.CharField(max_length=20)
    title = models.CharField(max_length=200)
    content = models.TextField()
    image = models.ImageField(upload_to='images/about/')
    url = models.URLField()
    static = models.ManyToManyField(Statictics)

    class Meta:
        verbose_name = 'Bosh sahifa statistikalar'
        verbose_name_plural = 'Bosh sahifa statistikalar'



class CtaArea(models.Model):
    min_content = models.CharField(max_length=20)
    title = models.CharField(max_length=200)
    sub_title = models.CharField(max_length=500)
    btn_name = models.CharField(max_length=200)
    btn_url = models.URLField(max_length=200)
    image = models.ImageField(upload_to='images/CtaArea/')


class AboutUs(models.Model):
    image = models.ImageField(upload_to='images/aboutUs/')
    title = models.CharField(max_length=200)
    content = models.TextField()
    t1 = models.CharField(max_length=100)
    c1 = models.CharField(max_length=100)
    t2 = models.CharField(max_length=100)
    c2= models.CharField(max_length=100)
    phone = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
     
    class Meta:
        verbose_name = 'Biz haqimizda'
        verbose_name_plural = 'Biz haqimizda'
 
class AServices(BaseModel):
    name = models.CharField(max_length=250)

    def __str__(self) -> str:
        return self.name
    
class AboutServices(BaseModel):
    min_content = models.CharField(max_length=20)
    title = models.CharField(max_length=200)
    content = models.TextField()
    image = models.ImageField(upload_to='AboutServices/')
    services = models.ManyToManyField(AServices)

    class Meta:
        verbose_name = 'Biz haqimizda servis'
        verbose_name_plural = 'Biz haqimizda servislar'



class Gallery(BaseModel):
    image = models.ImageField(upload_to='gallery')


class VideoGallery(BaseModel):
    image = models.ImageField(upload_to='gallery/video/')
    url = models.URLField()


class FAQ(BaseModel):
    question = models.CharField(max_length=200)
    answer = models.TextField()

    class Meta:
        verbose_name = 'Ko\'p so\'raladigan savol'
        verbose_name_plural = 'Ko\'p so\'raladigan savollar'

    def __str__(self) -> str:
        return self.question
    

class ReqCategory(BaseModel):
    name = models.CharField(max_length=200)
    

    def __str__(self) -> str:
        return self.name
    
class Requests(BaseModel):
    full_name = models.CharField(max_length=100)
    faculty_group = models.CharField(max_length=250)
    content = models.TextField()
    phone = models.CharField(max_length=50)
    subject = models.ForeignKey(ReqCategory,on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.full_name
    class Meta:
        verbose_name = 'Saytdan kelgan xabar'
        verbose_name_plural = 'Saytdan kelgan xabarlar'

class ContactImage(BaseModel):
    image = models.ImageField(upload_to='images/contact')
    class Meta:
        verbose_name = 'Bog\'lanish sahifasi rasm'

class ErrorImage(BaseModel):
    image = models.ImageField(upload_to='images/error')
    class Meta:
        verbose_name = 'Xatoliklar sahifasi rasm'


class TgBot(models.Model):
    bot_token = models.TextField()
    chat_id = models.CharField(max_length=200)

    def __str__(self):
        return self.chat_id