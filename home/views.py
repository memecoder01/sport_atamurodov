from django.shortcuts import render
from django.core.paginator import Paginator
# Create your views here.
from django.shortcuts import render
from .models import *
import datetime
import requests

from home.models import Requests
from home.models import TgBot

try:
    b = TgBot.objects.first()
    token = b.bot_token
    chat_id = b.chat_id
except:
    pass


def send_data(text):
    resp = requests.post(f'https://api.telegram.org/bot{token}/sendMessage?chat_id={chat_id}&text={text}&parse_mode=html')
    print(resp)

def get_info(uuid):
    t  = Requests.objects.filter(uuid=uuid).first()
    data = f"<b> Murojaat UUID:</b> {t.uuid} \n\n"
    data += f"<b> FIO:</b> {t.full_name}\n\n"
    data += f"<b> Fakultet va guruh:</b> {t.faculty_group}\n\n"
    data += f"<b> Telefon raqam:</b> {t.phone}\n\n"
    data += f"<b> Murojaat mavzusi:</b> {t.subject.name}\n\n"
    data += f"<b> Murojaat matni:</b> \n\n"
    data += f"<i>{t.content}</i>\n\n"
    data += f"Sana: {t.create_date}\n\n"
    return data
# Create your views here.
def home(request):
    context = {
        'homeinfo' : HomeInfo.objects.first(),
        'ATInfo' : ATInfo.objects.first(),
        'numbers' : Numbers.objects.all()[:4],
        'management' : Management.objects.all(),
        'abautAs' : AboutAS.objects.all()[:4],
        'events' : Events.objects.all().order_by('-date')[:6],
        'projects' : Projects.objects.all()[:6],
        'courses' : Courses.objects.all()[:3],
        'links' : UsefullLinks.objects.all(),
        'faq' : HomeFAQ.objects.first(),
        'HomeStasInfo' : HomeStasInfo.objects.first(),
        'CtaArea' : CtaArea.objects.first(),
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),

    }
    
    
    return render(request, 'home/home.html',context)


def events(request):
    
    
    if request.method == 'GET':
        print(request.GET)
        ct = request.GET.get('category',False)
        tg = request.GET.get('tag',False)
        if ct and Events.objects.filter(category__name=ct).exists():
            print("a")
            e = Events.objects.filter(category__name=ct).order_by('-date')
        elif (tg and Events.objects.filter(hash_tags__name=tg).exists()):
            e = Events.objects.filter(hash_tags__name=tg).order_by('-date')
            print("b")
        else:
            e = Events.objects.all().order_by('-date')
    paginator = Paginator(e, 4) 
    page_number = request.GET.get('page')
    events = paginator.get_page(page_number)
    context = {
        'posts' : events,
        'category' : EventCategory.objects.all(),
        'tags' : EventTag.objects.all(),
        'popular' : Events.objects.all()[:4],
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),
    }
    
    return render(request, 'home/blog.html',context)

def event_detail(request,uuid):
    context = {
        'event' : Events.objects.filter(uuid=uuid).first(),
        'category' : EventCategory.objects.all(),
        'tags' : EventTag.objects.all(),
        'popular' : Events.objects.exclude(uuid=uuid),
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),
        

    }
    
    return render(request, 'home/blog-details.html',context)


def about(request):
    context = {
        'AboutUs' : AboutUs.objects.first(),
        'management' : Management.objects.all(),
        'AboutAS' : AboutAS.objects.all(),
        'numbers' : Numbers.objects.all()[:4],
        'AboutServices' : AboutServices.objects.first(),
        'events' : Events.objects.all()[:4],
        'links' : UsefullLinks.objects.all(),
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),


    }
    
    return render(request, 'home/about.html',context)


def gallery(request):
    context = {
        'gallery' : Gallery.objects.all(),
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),
    }
    
    return render(request, 'home/gallery.html',context)

def video_gallery(request):
    context = {
        'gallery' : VideoGallery.objects.all(),
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),
    }
    
    return render(request, 'home/video_gallery.html',context)

def faq(request):
    if request.method == 'POST':
        post = request.POST
        full_name = post.get('full_name',False)
        faculty_group = post.get('faculty_group',False)
        content = post.get('content',False)
        phone = post.get('phone',False)
        subject = post.get('subject',False)
        if full_name and faculty_group and content and phone and subject:
            r = Requests.objects.create(full_name=full_name,faculty_group=faculty_group,content=content,phone=phone,subject_id=subject)
            r.save()
            send_data(get_info(r.uuid))
    context = {
        'faq' : FAQ.objects.all(),
        'subjects' : ReqCategory.objects.all(),
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),
    }
    
    return render(request, 'home/faq.html',context)

def contact(request):
    context = {}
    if request.method == 'POST':
        post = request.POST
        print(post)
        full_name = post.get('full_name',False)
        faculty_group = post.get('faculty_group',False)
        content = post.get('content',False)
        phone = post.get('phone',False)
        subject = post.get('subject',False)
        if full_name and faculty_group and content and phone and subject:
            r = Requests.objects.create(full_name=full_name,faculty_group=faculty_group,content=content,phone=phone,subject_id=subject)
            r.save()
            send_data(get_info(r.uuid))
            context['msg'] = "Ma'lumotlar yuborildi"
    context = {
        'subjects' : ReqCategory.objects.all(),
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),
        'backImage' : ContactImage.objects.first(),
    }
    
    return render(request, 'home/contact.html',context)
def error(request,exception):
    context = {
        'social' : Social.objects.all()[:4],
        'logo' : Logo.objects.first(),
        'contact' : Contact.objects.first(),
        'error' : ErrorImage.objects.first(),
    }
    return render(request, 'error.html',context)
