
from django.contrib import admin
from .models import *

admin.site.register(TgBot)

@admin.register(HomeInfo)
class HomeInfoAdmin(admin.ModelAdmin):
    pass

@admin.register(Social)
class SocialAdmin(admin.ModelAdmin):
    pass

@admin.register(HomeFAQ)
class HomeFAQAdmin(admin.ModelAdmin):
    pass

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    pass

@admin.register(AboutAS)
class AboutASAdmin(admin.ModelAdmin):
    pass

@admin.register(Management)
class ManagementAdmin(admin.ModelAdmin):
    pass

@admin.register(UsefullLinks)
class UsefullLinksAdmin(admin.ModelAdmin):
    pass

@admin.register(Direction)
class DirectionAdmin(admin.ModelAdmin):
    pass

@admin.register(ATInfo)
class ATInfoAdmin(admin.ModelAdmin):
    pass

@admin.register(Numbers)
class NumbersoAdmin(admin.ModelAdmin):
    pass

@admin.register(Events)
class EventsAdmin(admin.ModelAdmin):
    list_display = ["id","title"]
    list_display_links = ('id',"title")
    prepopulated_fields = {'slug':('title',),}
    save_as = True
    group_fieldsets = True

@admin.register(EventCategory)
class EventCategoryAdmin(admin.ModelAdmin):
    pass

@admin.register(EventTag)
class EventTagAdmin(admin.ModelAdmin):
    pass


@admin.register(Projects)
class ProjectsAdmin(admin.ModelAdmin):
    pass


@admin.register(CoursesDirection)
class CoursesDirectionAdmin(admin.ModelAdmin):
    pass

@admin.register(Courses)
class CoursesAdmin(admin.ModelAdmin):
    pass

@admin.register(Statictics)
class CoursesAdmin(admin.ModelAdmin):
    pass

@admin.register(HomeStasInfo)
class CoursesAdmin(admin.ModelAdmin):
    pass


@admin.register(CtaArea)
class CoursesAdmin(admin.ModelAdmin):
    pass


@admin.register(AboutUs)
class CoursesAdmin(admin.ModelAdmin):
    pass
@admin.register(AServices)
class CoursesAdmin(admin.ModelAdmin):
    pass
@admin.register(AboutServices)
class CoursesAdmin(admin.ModelAdmin):
    pass
@admin.register(Logo)
class CoursesAdmin(admin.ModelAdmin):
    pass

@admin.register(Gallery)
class CoursesAdmin(admin.ModelAdmin):
    pass


@admin.register(VideoGallery)
class CoursesAdmin(admin.ModelAdmin):
    pass


@admin.register(FAQ)
class CoursesAdmin(admin.ModelAdmin):
    pass

@admin.register(Requests)
class CoursesAdmin(admin.ModelAdmin):
    pass

@admin.register(ReqCategory)
class CoursesAdmin(admin.ModelAdmin):
    pass

@admin.register(ContactImage)
class CoursesAdmin(admin.ModelAdmin):
    pass


@admin.register(ErrorImage)
class CoursesAdmin(admin.ModelAdmin):
    pass