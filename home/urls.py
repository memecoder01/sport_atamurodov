from django.urls import path
from .views import *
urlpatterns = [
    path('', home, name='home'),
    path('about', about, name='about'),
    path('events', events, name='events'),
    path('event/<uuid:uuid>/', event_detail, name='event_detail'),
    path('gallery', gallery, name='gallery'),
    path('video/gallery', video_gallery, name='video_gallery'),
    path('faq/', faq, name='faq'),
    path('contact/', contact, name='contact'),
]