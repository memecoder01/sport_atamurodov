from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from admintion.models import TelegramBot
from users.models import CustomUser
import os
try:
    b = TelegramBot.objects.first()
    token = b.token
    chat_id = b.tgid
except: pass

# Create your views here.
@login_required
def basa(request):
    context = {}
    if request.method == "POST":
        if request.user.is_superuser:
            backup = request.POST.get('backup',False)
            git = request.POST.get('git',False)
            send = request.POST.get('send',False)
            if backup:
               backup_basa()
               
            if git:
                cwd = os.getcwd()
                os.system(f'{cwd}/admintion/git-commands.cmd')
                
            if send:
                text = request.POST.get('info')
                if text:
                   send_basa(text)
                else: send_basa()
    return render(request, 'admintion/basa.html',context)

def backup_basa():
    cwd = os.getcwd()
    os.system(f'{cwd}/admintion/backup.cmd')

def git():
    cwd = os.getcwd()
    os.system(f'{cwd}/admintion/git-commands.cmd')

def send_basa(text=''):
    import datetime
    import requests
    files = {'document':open("C:\\backups\\astr0-crm.sql","rb")}
    date = datetime.datetime.now()
    resp = requests.post(f'https://api.telegram.org/bot{token}/sendDocument?chat_id={chat_id}&caption={text + str(date)}',files=files)
    print(resp.status_code)


def password_again(request):
    context = {}
    if request.user.is_superuser:
        username = request.GET.get('username',False)
        if username and  CustomUser.objects.filter(phone=username).exists():
            user = CustomUser.objects.filter(phone=username).first()
            context['pass_user'] = user
            if request.method == "POST":
                password = request.POST.get('pass',False)
                if user and password:
                    user.set_password(password)
                    user.save()
                    context['msg'] = 'Parol yangilandi'
        else: context['err'] = 'Bunday foydalanuvchi mavjud emas'

        return render(request, 'admintion/password_again.html',context)
    else:
         return redirect('home')