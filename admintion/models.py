from django.db import models
from simple_history.models import HistoricalRecords
import uuid

from users.models import CustomUser
# Create your models here.
class BaseModel(models.Model):
    id = models.AutoField(unique=True,primary_key=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    history = HistoricalRecords(inherit=True)
    # admin_user = models.ForeignKey(CustomUser,on_delete=models.PROTECT,null=True,blank=True,editable=False,related_name="admin")
    class Meta:
        abstract = True


class TelegramBot(BaseModel):
    tgid = models.CharField(max_length=200)
    token = models.TextField()

    def __str__(self):
        return self.tgid

    